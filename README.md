# Kit

NOTE: This is an exploration. See discussion in [T333668](https://phabricator.wikimedia.org/T333668). I think it makes more sense to implement this in MediaWiki core.

A kit of tools for managing a MediaWiki installation.

## Usage

### Development

1. Clone this repository somewhere
2. Modify `composer.local.json` in MediaWiki's root directory, specifying path to this repo
3. `composer require --dev "mediawiki/kit @dev"` in MediaWiki root directory

### Future 
In a MediaWiki installation: `composer require --dev mediawiki/kit` 

## Features

- Download extensions/skins: `bin/kit get GrowthExperiments`
- [not yet implemented] Update core/extensions/skins
- [not yet implemented] Enable/disable extensions/skins
- [not yet implemented] Checkout gerrit patches with depends-on across repositories
- [not yet implemented] Reproduce CI build versions
- [not yet implemented] Set configuration

## Goal

Reinvent fewer wheels by providing a shared set of tools for common actions that one can reuse in variety of environments.

### Non-goals

* Managing external services (PHP, MySQL, ElasticSearch)
* Interacting with MediaWiki database directly. Invoking MediaWiki via maintenance scripts is fair game, though.
* Replacing MediaWiki maintenance scripts

## Design principles

* Written in PHP and with adherence to MediaWiki code style, for ease of contribution.
* Handle common use cases
* Performance
* Easy to debug and contribute to

## Prior art

- https://gerrit.wikimedia.org/r/c/mediawiki/core/+/308891
- https://gerrit.wikimedia.org/r/plugins/gitiles/wmf-utils/+/refs/heads/master
- get-code subcommand of mwcli
