<?php

namespace Mediawiki\Kit\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

#[AsCommand(
	name: 'get',
	description: 'Use git clone to download MediaWiki extension and skin code.',
	hidden: false,
)]
class GetCommand extends Command {

	protected function configure() {
		$this->addArgument( 'name', InputArgument::REQUIRED, 'Name of the skin or extension to download.' );
		$this->addOption(
			'type',
			null,
			InputArgument::OPTIONAL,
			'Whether the project is of "extensions" or "skins" type.',
			'extensions'
		);
		$this->addOption(
			'clone-method',
			null,
			InputArgument::OPTIONAL,
			'Whether to use "https" or "ssh" for cloning.',
			'https'
		);
		$this->addOption(
			'ssh-username',
			null,
			InputArgument::OPTIONAL,
			'Shell username to use with git clone. Defaults to GERRIT_USERNAME environment variable, if set.',
			getenv( 'GERRIT_USERNAME' )
		);
		$this->addOption(
			'clone-depth',
			null,
			InputArgument::REQUIRED,
			'Value to use with git --clone-depth',
		);
		$this->addOption(
			'mediawiki-root',
			null,
			InputArgument::OPTIONAL,
			'Path to root of MediaWiki directory.',
			getcwd()
		);
	}

	protected function execute( InputInterface $input, OutputInterface $output ): int {
		if ( !in_array( $input->getOption( 'type' ), [ 'extensions', 'skins' ] ) ) {
			$output->writeln( '<error>--type must be one of "extensions" or "skins"</error>' );
			return Command::FAILURE;
		}
		$projectName = $this->getProjectNameForGerritUrl( $input->getOption( 'type' ), $input->getArgument( 'name' ) );
		$cloneMethod = $input->getOption( 'clone-method' );
		$sshUsername = $input->getOption( 'ssh-username' );
		if ( $cloneMethod === 'ssh' && !$sshUsername ) {
			$output->writeln(
				'<error>To clone via SSH, specify the username with --ssh-username or 
set GERRIT_USERNAME={username} in your shell configuration (e.g. .bashrc, .zshrc) file.</error>'
			);
			return Command::FAILURE;
		}

		$gitCommand = [ 'git', 'clone' ];
		if ( $cloneMethod === 'ssh' ) {
			$gitCommand[] = sprintf( 'ssh://%s@gerrit.wikimedia.org:29418/%s', $sshUsername, $projectName );
		} else {
			$gitCommand[] = sprintf( 'https://gerrit.wikimedia.org/r/%s',  $projectName );
		}
		if ( $input->getOption( 'clone-depth' ) ) {
			$gitCommand[] = '--clone-depth=' . $input->getOption( 'clone-depth' );
		}
		$type = $input->getOption( 'type' );
		$path = $input->getOption( 'mediawiki-root' ) . '/' . $type;
		$cwd = realpath( $path );

		if ( $output->isVerbose() ) {
			$output->writeln( sprintf( 'Attempting to clone %s via %s in %s', $projectName, $cloneMethod, $cwd ) );
		}
		$process = new Process( $gitCommand, $cwd );
		$process->run( static function ( $type, $buffer ) use ( $output )  {
			if ( $output->isVerbose() ) {
				$output->writeln( $buffer );
			}
		} );
		if ( $process->getExitCode() !== 0 ) {
			$output->write( '<error>' . $process->getErrorOutput() . '</error>' );
			return Command::FAILURE;
		}
		$output->writeln( 'Successfully cloned ' . $projectName );

		return Command::SUCCESS;
	}

	/**
	 * @param string $type One of "extensions" or "skins"
	 * @param string $name
	 * @return string A string in the format "mediawiki/{extensions,skins}/{name}" for use with cloning from Gerrit.
	 */
	private function getProjectNameForGerritUrl( string $type, string $name ): string {
		return "mediawiki/$type/$name";
	}
}
